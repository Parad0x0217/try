#ifndef INVENTORY_HPP
#define INVENTORY_HPP

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>

#include <vector>
#include <string>

#include "World/Item.hpp"

class Inventory
{
public:
	//constructor
	Inventory();

	//deconstructor
	~Inventory();
private:

	std::vector<Item> Items;

};

#endif