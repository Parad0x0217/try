#ifndef TEXTBUTTON_HPP
#define TEXTBUTTON_HPP

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>

#include <string>

#include "Helpers/Math.hpp"
#include "Helpers/Typedefs.hpp"

class TextButton
{
public:

	TextButton(std::string Text, sf::Font& Font, int characterSize, sf::Color InitialColor, sf::Color SelectedColor, sf::Vector2f Position);

	sf::Text& getText();

	void doAction(sf::RenderWindow& window);

	bool Clicked();

	void Update(int MouseX, int MouseY);

	~TextButton();

private:

	sf::Color mInitialColor;
	sf::Color mSelectedColor;

	bool mClicked;

	sf::Text mText;
	sf::Vector2f mScale;
};

#endif