#ifndef BLOCK_HPP
#define BLOCK_HPP

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>

class Block
{
public:
	//constructor
	Block(sf::Vector2f Size, sf::Vector2f Position, int Type);

	//used to access and draw the individual block
	sf::RectangleShape& getBlock();

	//used to show the block has been selected
	void selectBlock(bool Select);

	//deconstructor
	~Block();

private:

	//shape of Block
	sf::RectangleShape* mRectShape;

	//color of Block
	sf::Color Color;

	//size and position of block
	sf::Vector2f mSize;
	sf::Vector2f mPosition;

	//type of block
	// 0 = grass
	// 1 = water
	// 2 = snow
	// 3 = dirt
	// 4 = wall
	int mType;

};

#endif