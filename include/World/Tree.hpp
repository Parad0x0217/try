#ifndef TREE_HPP
#define TREE_HPP

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>

class Tree
{
public:
	//constructor
	Tree(sf::Vector2f Size, sf::Vector2f Position);

	//used to access and draw the individual Tree
	sf::ConvexShape& getTree();

	//deconstructor
	~Tree();

private:

	//shape of Tree
	sf::ConvexShape* mTreeShape;

	//size and position of Tree
	sf::Vector2f mSize;
	sf::Vector2f mPosition;
};

#endif