#ifndef WORLD_HPP
#define WORLD_HPP

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>

#include <iostream>

#include <noise.h>

#include <boost/thread.hpp>
#include <boost/foreach.hpp>

#include <vector>

#include "World/Block.hpp"
#include "World/Tree.hpp"
#include "Player/Player.hpp"
#include "Helpers/Globals.hpp"

class World
{
public:
	//constructor
	World(sf::Vector2f Scale, sf::Vector2f Size, sf::RenderWindow& window);

	//deconstructor
	~World();

	//start the thread!
	void Start();

	//join thread
	void Join();

	//handle input
	void HandleInput(sf::Event& event);

	//test to see if generation was OK
	int isGenerationSuccessful();

	//draw the world on screen
	void Draw();

	//updates the map according to player position
	void Update();

private:

	float z;
	sf::View currentView;

	//Pointer to the window
	sf::RenderWindow* mWindow;

	//Player
	Player* mPlayer;

	//Initialize
	void Init();

	//0 = success
	//-1 = failure
	int generatedSuccessfully;

	//Scale of each block
	sf::Vector2f mScale;

	//Size of world
	sf::Vector2f mSize;

	//vector of blocks in the world
	std::vector<Block*> Blocks;

	//Trees!
	std::vector<Tree*> Trees;

	//Possible player positions
	std::vector<sf::Vector2f> PossiblePositions;
	sf::Vector2f PlayerPosition;

	//thread yo!
	boost::thread mThread; 

	//sync
	boost::mutex mMutex;
};

#endif