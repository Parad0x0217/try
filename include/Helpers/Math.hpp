#ifndef MATH_HPP
#define MATH_HPP

class Math
{
public:

	static float Lerp(float value, float start, float end) 
	{ 
		return start + (end - start) * value; 
	}

};

#endif