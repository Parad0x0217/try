#ifndef PLAYER_HPP
#define PLAYER_HPP

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>

class Player
{
public:
	//Scale of the player, initial position of the player
	Player(sf::Vector2f Scale, sf::Vector2f Position, sf::Color Color, sf::RenderWindow& window);

	//deconstructor
	~Player();

	//function to handle input
	void HandleInput(sf::Event& event);

	//update things such as position, rotation, etc
	void Update();

	//draw the player on screen
	void Draw();

	//returns position of player
	sf::Vector2f getPosition();

	//set position of player (relative to world)
	void setPosition(sf::Vector2f Position);

	//move player relative to self
	void move(sf::Vector2f Position);

private:

	//pointer to window
	sf::RenderWindow* mWindow;

	//shape of player
	sf::RectangleShape* mRectShape;

	//Scale and position of player
	sf::Vector2f mScale;
	sf::Vector2f mPosition;

	//color of player
	sf::Color mColor;

};

#endif