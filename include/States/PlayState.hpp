#ifndef PLAYSTATE_HPP
#define PLAYSTATE_HPP

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>

#include "Player/Player.hpp"
#include "World/World.hpp"

#include <boost/thread.hpp>

class PlayState
{
public:

	//constructors
	PlayState(sf::RenderWindow& window);

	//deconstructor
	~PlayState();
	
	//Initialize game with size of world and scale
	void Init(int Size, sf::Vector2f Scale);

	//run ze game!
	void Run();

private:

	//draw
	void Draw();

	//update
	void Update();

	//get input junk!
	void HandleInput(sf::Event& event);

	//pointer to our renderwindow
	sf::RenderWindow* mWindow;

	//our world
	World* mWorld;
};

#endif