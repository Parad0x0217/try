#ifndef MENUSTATE_HPP
#define MENUSTATE_HPP

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>

#include <vector>

#include "UI/TextButton.hpp"
#include "UI/Font.hpp"
#include "Helpers/Math.hpp"

class MenuState
{
public:
	//constructor
	MenuState(sf::RenderWindow& window);

	//allow buttons to be added
	void addButton(TextButton* textButton);

	//run the whole thing!
	void Run();

	//deconstructor
	~MenuState();

private:

	//position of mouse
	int mMouseX;
	int mMouseY;

	//draw the menu
	void Draw();
	
	//update the menu
	void Update();
	
	//get input junk!
	void HandleInput(sf::Event& event);

	//pointer to the window
	sf::RenderWindow* mWindow;

	//all the buttons
	std::vector<TextButton*> mButtons;
};

#endif