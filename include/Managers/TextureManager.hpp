#ifndef TEXTUREMANAGER_HPP
#define TEXTUREMANAGER_HPP

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>

#include <string>
#include <iostream>

#include <boost/ptr_container/ptr_map.hpp>

class TextureManager
{
public:
	//constructor
	TextureManager();

	//deconstructor
	~TextureManager();

	//load new Texture
	//returns Texture if it is already loaded
	sf::Texture* loadTexture(std::string Name, std::string Location);
	//overload loadResouce with other types

private:

	// map containing name and Texture
	boost::ptr_map<std::string, sf::Texture> mTextureList;

};

#endif