#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>

#include "States/MenuState.hpp"
#include "States/StartMenuState.hpp"
#include "States/PlayState.hpp"
#include "States/PauseState.hpp"

#include "Helpers/Callbacks.hpp"
#include "Helpers/Globals.hpp"

int main()
{
	//create new gamestate and initialize it
	//to start with the main menu
	int* gameState = new int(States::MAINMENU);
	int* gameSize = new int(-1);

	//create new renderwindow
	sf::RenderWindow* window = new sf::RenderWindow(sf::VideoMode(800, 600, 32), "Try", sf::Style::Close);
	//sf::RenderWindow* window = new sf::RenderWindow(sf::VideoMode::GetDesktopMode(), "Try", sf::Style::Fullscreen);
	window->EnableVerticalSync(true);

	window->ShowMouseCursor(true);

	//load font for menus
	sf::Font mFont = sf::Font();
	mFont.LoadFromMemory(Font, Font_Size);

	MenuState mm = MenuState(*window);
	mm.addButton(new TextButton("Start", mFont, 72, sf::Color::White, sf::Color::Green, sf::Vector2f((float)window->GetWidth()/2, (float)window->GetHeight()/2-200)));
	mm.addButton(new TextButton("Load", mFont, 72, sf::Color::White, sf::Color::Blue, sf::Vector2f((float)window->GetWidth()/2, (float)window->GetHeight()/2-100)));
	mm.addButton(new TextButton("Options", mFont, 72, sf::Color::White, sf::Color::Yellow, sf::Vector2f((float)window->GetWidth()/2, (float)window->GetHeight()/2)));
	mm.addButton(new TextButton("Quit", mFont, 72, sf::Color::White, sf::Color::Red, sf::Vector2f((float)window->GetWidth()/2, (float)window->GetHeight()/2+100)));

	PlayState ps = PlayState(*window);

	while (window->IsOpen())
	{
		//do!
	}

	return 0;
}