#include "Player/Player.hpp"

Player::Player(sf::Vector2f Scale, sf::Vector2f Position, sf::Color Color, sf::RenderWindow& window)
{
	//set local variables
	mScale = Scale;
	mPosition = Position;
	mColor = Color;
	mWindow = &window;

	//create new rectangle shape for player
	mRectShape = new sf::RectangleShape(mScale);

	//set origin at exact center
	//mRectShape->SetOrigin(mRectShape->GetScale().x/2, mRectShape->GetScale().y/2);

	//move to initial position
	mRectShape->SetPosition(mPosition);

	//set player color
	mRectShape->SetFillColor(mColor);
}

Player::~Player()
{
	//clean up
	delete mRectShape;
	mRectShape = NULL;
}

sf::Vector2f Player::getPosition()
{
	return mPosition;
}

void Player::setPosition(sf::Vector2f Position)
{
	mPosition = Position;
}

void Player::move(sf::Vector2f Position)
{
	mPosition += Position;
}

void Player::HandleInput(sf::Event& event)
{
	// W key pressed: move up
	if ((event.Type == sf::Event::KeyPressed) && (event.Key.Code == sf::Keyboard::W))
	{
		mPosition.y -= mScale.y;
		mWindow->SetView(sf::View(mWindow->GetView().GetCenter()-sf::Vector2f(0, mScale.y), mWindow->GetView().GetSize()));
	}
	// A key pressed: move left
	if ((event.Type == sf::Event::KeyPressed) && (event.Key.Code == sf::Keyboard::A))
	{
		mPosition.x -= mScale.x;
		mWindow->SetView(sf::View(mWindow->GetView().GetCenter()-sf::Vector2f(mScale.x, 0), mWindow->GetView().GetSize()));

	}
	// S key pressed: move down
	if ((event.Type == sf::Event::KeyPressed) && (event.Key.Code == sf::Keyboard::S))
	{
		mPosition.y += mScale.y;
		mWindow->SetView(sf::View(mWindow->GetView().GetCenter()-sf::Vector2f(0, -mScale.y), mWindow->GetView().GetSize()));

	}
	// D key pressed: move right
	if ((event.Type == sf::Event::KeyPressed) && (event.Key.Code == sf::Keyboard::D))
	{
		mPosition.x += mScale.x;
		mWindow->SetView(sf::View(mWindow->GetView().GetCenter()-sf::Vector2f(-mScale.x, 0), mWindow->GetView().GetSize()));
	}
}

void Player::Update()
{
	//set to updated position
	mRectShape->SetPosition(mPosition);
}

void Player::Draw()
{
	//draw the player
	mWindow->Draw(*mRectShape);
}