#include "States/MenuState.hpp"

MenuState::MenuState(sf::RenderWindow& window)
{
	mWindow = &window;

	mMouseX = 0;
	mMouseY = 0;
}

MenuState::~MenuState()
{
	delete mWindow;
	mWindow = NULL;
}

void MenuState::addButton(TextButton* textButton)
{
	mButtons.push_back(textButton);
}

void MenuState::Run()
{
	// Handle events
	sf::Event event;
	while (mWindow->PollEvent(event))
	{
		// Window closed: exit
		if (event.Type == sf::Event::Closed)
		{
			mWindow->Close();
			break;
		}		

		//handle input for the player
		HandleInput(event);
	}

	//update
	Update();

	// Clear the window
	mWindow->Clear(sf::Color(0, 0, 0));

	//draw
	Draw();

	// Display things on screen
	mWindow->Display();
}

void MenuState::Draw()
{
	for (std::vector<TextButton*>::iterator i = mButtons.begin(); i != mButtons.end(); ++i)
	{
		mWindow->Draw((*i)->getText());
	}
}

void MenuState::Update()
{
	for (std::vector<TextButton*>::iterator i = mButtons.begin(); i != mButtons.end(); ++i)
	{
		(*i)->Update(mMouseX, mMouseY);

		if ((*i)->Clicked())
		{
			(*i)->doAction(*mWindow);
		}
	}
}

void MenuState::HandleInput(sf::Event& event)
{
	//update mouse position
	if (event.Type == sf::Event::MouseMoved)
	{
		mMouseX = event.MouseMove.X;
		mMouseY = event.MouseMove.Y;
	}
}