#include "States/PlayState.hpp"

PlayState::PlayState(sf::RenderWindow& window)
{
	mWindow = &window;
}

PlayState::~PlayState()
{
	//clean up pointers

	delete mWorld;
	mWorld = NULL;

	delete mWindow;
	mWindow = NULL;
}

void PlayState::Init(int Size, sf::Vector2f Scale)
{
	//create new world
	do
	{
		switch (Size)
		{
		case Size::SMALL:
			mWorld = new World(Scale, sf::Vector2f(1024, 1024), *mWindow);
			break;

		case Size::MEDIUM:
			mWorld = new World(Scale, sf::Vector2f(2048, 2048), *mWindow);
			break;

		case Size::LARGE:
			mWorld = new World(Scale, sf::Vector2f(4096, 4096), *mWindow);
			break;

		case Size::HUGE:
			mWorld = new World(Scale, sf::Vector2f(8192, 8192), *mWindow);
			break;

		case Size::WINDOW_DIMENSIONS:
			mWorld = new World(Scale, sf::Vector2f((float)mWindow->GetWidth(), (float)mWindow->GetHeight()), *mWindow);
			break;
		}

		//start the thread and generate a new world
		mWorld->Start();
		mWorld->Join();

		//if generation is not successful
		//delete the messed up world and try again
		if (mWorld->isGenerationSuccessful() != 0)
		{
			delete mWorld;
		}
	}
	while (mWorld->isGenerationSuccessful() != 0);

}

void PlayState::Run()
{
	// Handle events
	sf::Event event;
	while (mWindow->PollEvent(event))
	{

		// Window closed: exit
		if (event.Type == sf::Event::Closed)
		{
			mWindow->Close();
			break;
		}		

		HandleInput(event);
	}

	Update();

	// Clear the mWindow
	mWindow->Clear(sf::Color(0, 0, 0));

	Draw();

	// Display things on screen
	mWindow->Display();

}

void PlayState::Draw()
{
	//draw the world
	mWorld->Draw();
}

void PlayState::Update()
{
	//update world
	mWorld->Update();
}

void PlayState::HandleInput( sf::Event& event )
{
	//handle input for the player
	mWorld->HandleInput(event);
}