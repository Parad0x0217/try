#include "UI/TextButton.hpp"

TextButton::TextButton(std::string Text, sf::Font& Font, int characterSize, sf::Color InitialColor, sf::Color SelectedColor, sf::Vector2f Position)
{
	mScale = sf::Vector2f(0, 0);

	mInitialColor = InitialColor;
	mSelectedColor = SelectedColor;

	mClicked = false;

	mText = sf::Text(Text, Font, characterSize);
	mText.SetColor(mInitialColor);
	mText.SetOrigin(mText.GetLocalBounds().Width/2, mText.GetLocalBounds().Height/2);
	mText.SetPosition(Position);
}

TextButton::~TextButton()
{
}

sf::Text& TextButton::getText()
{
	return mText;
}

void TextButton::doAction(sf::RenderWindow& window)
{
	//do something..
}

bool TextButton::Clicked()
{
	return mClicked;
}

void TextButton::Update(int MouseX, int MouseY)
{
	if (mText.GetGlobalBounds().Contains((float)MouseX, (float)MouseY))
	{
		mText.SetColor(mSelectedColor);
		mScale.x = Math::Lerp(0.1f, mScale.x, 2.0f);
		mScale.y = Math::Lerp(0.1f, mScale.y, 2.0f);
		mText.SetScale(mScale);

		if (sf::Mouse::IsButtonPressed(sf::Mouse::Left))
			mClicked = true;
	}
	else
	{
		mText.SetColor(mInitialColor);
		mScale.x = Math::Lerp(0.1f, mScale.x, 1.0f);
		mScale.y = Math::Lerp(0.1f, mScale.y, 1.0f);
		mText.SetScale(mScale);

		mClicked = false;
	}
}