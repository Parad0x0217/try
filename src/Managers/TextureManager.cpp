#include "Managers/TextureManager.hpp"

TextureManager::TextureManager()
{
}

TextureManager::~TextureManager()
{
}

sf::Texture* TextureManager::loadTexture(std::string Name, std::string Location)
{
	boost::ptr_map<std::string, sf::Texture>::iterator iter;

	for (iter = mTextureList.begin(); iter != mTextureList.end(); ++iter)
	{
		if (iter->first == Name)
		{
			//std::cout << "Item found! Returning: " << iter->first << std::endl;
			return iter->second;
		}
	}

	//std::cout << "Item not found! Returning: " << Name << std::endl;
	sf::Texture* Texture = new sf::Texture();
	Texture->LoadFromFile(Location);
	mTextureList.insert(Name, Texture);

	return Texture;
}