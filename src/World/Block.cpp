#include "World/Block.hpp"

Block::Block(sf::Vector2f Size, sf::Vector2f Position, int Type)
{
	//set local variables
	mSize = Size;
	mPosition = Position;
	mType = Type;

	//create new rectangle shape for Block
	mRectShape = new sf::RectangleShape(mSize);

	//set origin at exact center
	//mRectShape->SetOrigin(mRectShape->GetSize().x/2, mRectShape->GetSize().y/2);

	//move to initial position
	mRectShape->SetPosition(mPosition);

	switch (mType)
	{
	case 0: //grass
		mRectShape->SetFillColor(sf::Color(50, 200, 50));
		break;
	case 1: //water
		mRectShape->SetFillColor(sf::Color(50, 100, 255));
		break;
	case 2: //snow
		mRectShape->SetFillColor(sf::Color(255, 255, 255));
		break;
	case 3: //dirt
		mRectShape->SetFillColor(sf::Color(100, 50, 0));
		break;
	case 4: //deep water
		mRectShape->SetFillColor(sf::Color(0, 46, 184));
		break;
	default: //if other number given, it is black
		mRectShape->SetFillColor(sf::Color(0, 0, 0));
		break;
	}

	Color = mRectShape->GetFillColor();
}

Block::~Block()
{
	//clean up
	delete mRectShape;
	mRectShape = NULL;
}

sf::RectangleShape& Block::getBlock()
{
	//return the block
	return *mRectShape;
}

void Block::selectBlock(bool Select)
{
	if (Select)
	{
		mRectShape->SetFillColor(Color*sf::Color(200, 200, 200));
	}
	else
	{
		mRectShape->SetFillColor(Color);
	}
}