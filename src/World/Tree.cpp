#include "World/Tree.hpp"

Tree::Tree(sf::Vector2f Size, sf::Vector2f Position)
{
	//set local variables
	mSize = Size;
	mPosition = Position;

	//create new rectangle shape for Tree
	mTreeShape = new sf::ConvexShape(3);

	//initialize tree (triangle) points
	mTreeShape->SetPoint(0, sf::Vector2f(0, mSize.y));
	mTreeShape->SetPoint(1, sf::Vector2f(mSize.x/2, 0));
	mTreeShape->SetPoint(2, sf::Vector2f(mSize.x, mSize.y));
	mTreeShape->SetFillColor(sf::Color(0, 128, 0));

	//set origin at exact center
	//mRectShape->SetOrigin(mRectShape->GetSize().x/2, mRectShape->GetSize().y/2);

	//move to initial position
	mTreeShape->SetPosition(mPosition);
}

Tree::~Tree()
{
	//clean up
	delete mTreeShape;
	mTreeShape = NULL;
}

sf::ConvexShape& Tree::getTree()
{
	//return the Tree
	return *mTreeShape;
}