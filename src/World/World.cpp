#include "World/World.hpp"

World::World(sf::Vector2f Scale, sf::Vector2f Size, sf::RenderWindow& window)
{
	mScale = Scale;
	mSize = Size;
	mWindow = &window;
	generatedSuccessfully = -1;
	z = 1.0f;
}

World::~World()
{
	//BOOST_FOREACH(Tree* t, Trees)
	//{
	//	delete t;
	//	t = NULL;
	//}

	//BOOST_FOREACH(Block* b, Blocks)
	//{
	//	delete b;
	//	b = NULL;
	//}

	delete mPlayer;
	delete mWindow;
	mPlayer = NULL;
	mWindow = NULL;

	for (std::vector<Tree*>::iterator i = Trees.begin(); i != Trees.end(); )
	{
		delete (*i);
		(*i) = NULL;
		++i;
	}

	for (std::vector<Block*>::iterator i = Blocks.begin(); i != Blocks.end(); )
	{
		delete (*i);
		(*i) = NULL;
		++i;
	}
}

void World::Init()
{
	noise::module::Perlin myModule;
	myModule.SetOctaveCount(4);
	myModule.SetFrequency(2);
	myModule.SetPersistence(0.5);
	//myModule.SetLacunarity(1.5);

	std::srand(static_cast<unsigned int>(std::time(NULL)));
	myModule.SetSeed(rand());

	noise::model::Plane myPlane;
	myPlane.SetModule(myModule);

	for (float x = 0; x < mSize.x; x += mScale.x)
	{
		for (float y = 0; y < mSize.y; y += mScale.y)
		{
			float Val = (float)myPlane.GetValue(x/mSize.x, y/mSize.y);

			if (Val >= 0.0 && Val <= 0.1) //dirt
			{
				Blocks.push_back(new Block(mScale, sf::Vector2f(x, y), 3 ));
			}
			else if (Val > 0.1 && Val <= 0.8) //grass
			{
				Blocks.push_back(new Block(mScale, sf::Vector2f(x, y), 0 ));

				//add trees by base of mountains, eh!
				if (Val > 0.7 && Val <= 0.8)
				{
					//push back a new tree!
					Trees.push_back(new Tree(mScale, sf::Vector2f(x, y)));
				}

				if (Val > 0.3 && Val <= 0.5)
				{
					//add grass locations to later be randomly selected
					PossiblePositions.push_back(sf::Vector2f(x, y));
				}
			}
			else if (Val > 0.8) //snow
			{
				Blocks.push_back(new Block(mScale, sf::Vector2f(x, y), 2 ));
			}
			else if (Val >= -0.5 && Val < 0.0) //water
			{
				Blocks.push_back(new Block(mScale, sf::Vector2f(x, y), 1 ));
			}
			else if (Val < -0.5) //deep water
			{
				Blocks.push_back(new Block(mScale, sf::Vector2f(x, y), 4 ));
			}
		}
	}

	//store a random position that is grass
	if (PossiblePositions.size() > 0) //success! more than 0 locations to walk
	{
		PlayerPosition = PossiblePositions.at(rand()%PossiblePositions.size());
		generatedSuccessfully = 0;
		//create new player
		mPlayer = new Player(mScale, PlayerPosition, sf::Color(255, 50, 50), *mWindow);
		mWindow->SetView(sf::View(PlayerPosition, mWindow->GetView().GetSize()));
		//std::cout << "Success Seed: " << myModule.GetSeed() << std::endl;
	}
	else //failure! no where to move!
	{
		generatedSuccessfully = -1;
		//std::cout << "Fail Seed: " << myModule.GetSeed() << std::endl;
	}
}

void World::Start()  
{  
	boost::unique_lock<boost::mutex> lock(mMutex);
    mThread = boost::thread(&World::Init, this);  
}

void World::Join()
{
	mThread.join();
}

int World::isGenerationSuccessful()
{
	return generatedSuccessfully;
}

void World::Draw()
{
	//draw the world
	currentView = sf::View(mWindow->GetView().GetCenter(), mWindow->GetView().GetSize());
	
	//iterate through all blocks
	for (std::vector<Block*>::iterator i = Blocks.begin(); i != Blocks.end(); )
	{
		//if block is offscreen, do not draw it.
		if ((*i)->getBlock().GetPosition().x > mPlayer->getPosition().x + currentView.GetSize().x/2 ||
			(*i)->getBlock().GetPosition().x < mPlayer->getPosition().x - currentView.GetSize().x/2 ||
			(*i)->getBlock().GetPosition().y > mPlayer->getPosition().y + currentView.GetSize().y/2 ||
			(*i)->getBlock().GetPosition().y < mPlayer->getPosition().y - currentView.GetSize().y/2 - mScale.x)
		{
			++i;
		}
		else //block is visible on the screen
		{
			//draw and iterate!
			mWindow->Draw((*i)->getBlock());
			++i;
		}
	}

	//iterate through all trees and draw
	for (std::vector<Tree*>::iterator i = Trees.begin(); i != Trees.end(); )
	{
		//if tree is offscreen, do not draw it.
		if ((*i)->getTree().GetPosition().x > mPlayer->getPosition().x + currentView.GetSize().x/2 ||
			(*i)->getTree().GetPosition().x < mPlayer->getPosition().x - currentView.GetSize().x/2 ||
			(*i)->getTree().GetPosition().y > mPlayer->getPosition().y + currentView.GetSize().y/2 ||
			(*i)->getTree().GetPosition().y < mPlayer->getPosition().y - currentView.GetSize().y/2 - mScale.x)
		{
			++i;
		}
		else
		{
			mWindow->Draw((*i)->getTree());
			++i;
		}
	}

	mPlayer->Draw();
}

void World::Update()
{
	mPlayer->Update();
}

void World::HandleInput(sf::Event& event)
{
	mPlayer->HandleInput(event);
}